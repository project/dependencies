dependencies.module

DESCRIPTION

When enabled, checks dependencies for modules, providing user feedback at
admin/modules.

At present the module comes with a set of dependency information. It will
be refactored to read package metadata files.

Based on code by Chad Phillips (hunmunk).

TODO

Refactor to read metadata files.
Write javascript file for client-side checking.